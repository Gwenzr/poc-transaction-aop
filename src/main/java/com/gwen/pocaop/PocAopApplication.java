package com.gwen.pocaop;

import com.gwen.pocaop.entity.Voiture;
import com.gwen.pocaop.service.VoitureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocAopApplication.class, args);
    }

}
