package com.gwen.pocaop.controller;

import com.gwen.pocaop.entity.Voiture;
import com.gwen.pocaop.service.VoitureService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/voiture")
public class VoitureController {

  private VoitureService voitureService;

    public VoitureController(VoitureService voitureService) {
        this.voitureService = voitureService;
    }


    @GetMapping("/commit")
    public String commit(){
        Voiture voiture = new Voiture();
        voiture.setCarburant("diesel");
        voiture.setMarque("renault");
        voitureService.shouldCommit(voiture);
        return "voiture créée avec succès";
    }

    @GetMapping("/rollback")
    public void rollback(){
        voitureService.shouldRollback(new Voiture());
    }
}
