package com.gwen.pocaop.service;

import com.gwen.pocaop.entity.Voiture;
import com.gwen.pocaop.repository.VoitureRepository;
import org.springframework.stereotype.Service;

@Service
public class VoitureService {

    private VoitureRepository voitureRepository;

    public VoitureService(VoitureRepository voitureRepository) {
        this.voitureRepository = voitureRepository;
    }

    public void shouldCommit(Voiture voitureAAjouter) {
        voitureRepository.save(voitureAAjouter);
    }

    public void shouldRollback(Voiture voitureAAjouter) {
        voitureRepository.save(voitureAAjouter);
        throw new RuntimeException();
    }

}
